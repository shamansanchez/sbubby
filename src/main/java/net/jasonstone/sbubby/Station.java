package net.jasonstone.sbubby;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.Player;
import org.bukkit.material.Lever;

public class Station {
	public Location center;
	public String name;

	public List<Location> levers;
	public World world;

	public Station(String name, int x, int y, int z) {
		world = Bukkit.getWorld("world_nether");
		center = new Location(world, x, y, z);
		this.name = name;

		levers = new ArrayList<Location>();
		levers.add(new Location(world, x, y, z - 1));
		levers.add(new Location(world, x, y, z + 1));
		levers.add(new Location(world, x + 1, y, z));
		levers.add(new Location(world, x - 1, y, z));
	}

	public boolean isLoaded() {
		return world.isChunkLoaded(center.getBlockX() >> 4, center.getBlockZ() >> 4);
	}

	public void SetPowered(boolean power) {
		for (Location l : levers) {
			Block block = l.getBlock();
			try {
				Switch lever = (Switch) block.getBlockData();
				lever.setPowered(power);
				block.setBlockData(lever);
			} catch (ClassCastException e) {
				continue;
			}
		}
	}

	public Player getClosestPlayer() {
		double closest = 100000.0;
		Player closestPlayer = null;
		for (Player p : Bukkit.getOnlinePlayers()) {
			Location playerLocation = p.getLocation();

			if (playerLocation.getWorld() != world) {
				continue;
			}

			if (playerLocation.distance(center) < closest) {
				closestPlayer = p;
				closest = playerLocation.distance(center);
			}
		}

		return closestPlayer;

	}
}
